
import os
import logging as log
from time import time

from openvino.inference_engine import IECore

from .common import resize_frames

class Model:

    class RequestBatch:
        def __init__(self):
            self.ids = []
            self.outputs = {}
            self.perf_stats = {}
            self.is_done = False

    def __init__(self, ie, device, name, xml_path, queue_size=1, logger=log.info):
        logger("Initialzing {} model".format(name))
        bin_path = os.path.splitext(xml_path)[0] + ".bin"
        self.net = ie.read_network(model=xml_path, weights=bin_path)

        if device == "AUTO":
            if "MYRIAD" in ie.available_devices:
                self.device = "MYRIAD"
            else:
                self.device = "CPU"
            logger("Auto-selected device for {}: {}".format(name, self.device))
        else:
            self.device = device

        self.in_blob_name = next(iter(self.net.input_info))
        self.out_blob_name = next(iter(self.net.outputs))
        self.net.batch_size = 1

        self.max_requests = queue_size
        self.active_requests = 0
        self.next_request_id = 0

        self.curr_batch = Model.RequestBatch()
        self.prev_batch = Model.RequestBatch()

        logger("Load {} model to the device".format(name))
        self.exec_net = ie.load_network(network=self.net, device_name=self.device, num_requests=self.max_requests)

    # TODO: Add shorter start function for one input maybe?

    def start_async(self, inputs):
        input_shape = self.net.input_info[self.in_blob_name].input_data.shape
        inputs = resize_frames(inputs, input_shape)

        self.prev_batch, self.curr_batch = self.curr_batch, self.prev_batch

        self.curr_batch = Model.RequestBatch()

        for input in inputs:
            request_id = self.enqueue(input)

            if request_id == -1:
                break

            self.curr_batch.ids.append(request_id)

    def enqueue(self, input):
        if self.active_requests >= self.max_requests:
            log.warning("Request processing rejected - too many requests")
            return -1

        # Selecting id for enqueued request
        request_id = self.next_request_id
        self.next_request_id += 1
        self.next_request_id %= self.max_requests

        self.exec_net.start_async(request_id, { self.in_blob_name: input })

        self.active_requests += 1

        return request_id

    def wait_for_previous_batch(self, timeout=None):
        return self.wait_for_batch(self.prev_batch, timeout)

    def wait_for_batch(self, batch, timeout=None):
        if self.active_requests <= 0 or batch.is_done:
            return

        # Default is "Infer not started" code for new created batch that has no ids
        stat_code = -11
        for i in batch.ids:
            stat_code = self.exec_net.requests[i].wait(timeout)

            if stat_code != 0:
                break

            batch.outputs[i] = {key: blob.buffer for key, blob in self.exec_net.requests[i].output_blobs.items()}
            batch.perf_stats[i] = self.exec_net.requests[i].get_perf_counts()

        if timeout == None or timeout == -1:
            self.active_requests -= len(batch.ids)

            batch.is_done = True

        return stat_code

    def get_outputs(self):
        # We're also waiting for previous batch to make sure all requests were completed
        # when getting outputs synchronously, so it couldn't screw up the further inferences
        self.wait_for_batch(self.prev_batch)
        self.wait_for_batch(self.curr_batch)

        # TODO: Need to optimize
        return [output[self.out_blob_name][0] for i, output in self.curr_batch.outputs.items()]

    def get_raw_previous_outputs(self):
        self.wait_for_batch(self.prev_batch)

        return list(self.prev_batch.outputs.values())

    def get_net(self):
        return self.net

    def get_in_blob_name(self):
        return self.in_blob_name

    def get_out_blob_name(self):
        return self.out_blob_name

    def get_input_shape(self):
        return self.net.input_info[self.in_blob_name].input_data.shape

    def get_performance_stats(self):
        return self.curr_batch.perf_stats
