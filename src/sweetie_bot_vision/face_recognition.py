
import time
import logging as log
from typing import NamedTuple

import cv2 as cv
import numpy as np
import scipy.spatial.distance
from scipy.optimize import linear_sum_assignment

from openvino.inference_engine import IECore

from .model import Model
from .common import *

from .byte_track.byte_tracker import BYTETracker

class FaceRecognizerConfig(NamedTuple):

    detection_model_xml : str
    reident_model_xml   : str
    landmarks_model_xml : str

    prob_threshold : float
    roi_scale_factor : float
    z_projection_scale : float
    min_roi_size : int
    device : str

# Taken from the description of the model:
# intel_models/face-reidentification-retail-0095
REFERENCE_LANDMARKS = [
    (30.2946 / 96, 51.6963 / 112), # left eye
    (65.5318 / 96, 51.5014 / 112), # right eye
    (48.0252 / 96, 71.7366 / 112), # nose tip
    (33.5493 / 96, 92.3655 / 112), # left lip corner
    (62.7299 / 96, 92.2041 / 112)] # right lip corner

class BYTE_Args(NamedTuple):
    track_thresh : float = 0.5
    track_buffer : int   = 3000
    match_thresh : float = 0.8
    min_box_area : float = 10

class FaceRecognizer:
    QUEUE_SIZE = 16

    def __init__(self, inference_engine, config, frame_size, logger=log.info, errlogger=log.error):

        self.ie = inference_engine

        self.config = config
        self.device = config.device
        self.prob_threshold = config.prob_threshold
        self.roi_scale_factor = config.roi_scale_factor
        self.min_roi_size = config.min_roi_size
        self.frame_size = frame_size

        self.features_distance_treshold = 0.7
            
        self.face_tracker = BYTETracker(BYTE_Args(), frame_rate=30, logger=errlogger)

        assert config.detection_model_xml != "", "You're not specified path to face detection model"
        assert config.landmarks_model_xml != "", "You're not specified path to landmarks regression model"
        assert config.reident_model_xml != "", "You're not specified path to face reidentification model"

        # @Hack: We should do this one abstraction layer higher. Just need mutable data class for config.
        # @Note: Assuming there's only two MYRIAD devices
        # Selecting device for auto mode
        if self.device == "AUTO":
            myriad_count = 0
            for device_name in self.ie.available_devices:
                if "MYRIAD" in device_name:
                    myriad_count += 1
                    # Choose first device in the list
                    if myriad_count == 1:
                        self.device = device_name
                        logger("Auto-selected device for face recognizer: {}".format(self.device))
                        break
            

        self.face_detector = Model(self.ie, self.device, "face detection", config.detection_model_xml, 2, logger=logger)
        self.landmarks_detector = Model(self.ie, self.device, "landmarks detection", config.landmarks_model_xml, self.QUEUE_SIZE, logger=logger)
        self.face_identifier = Model(self.ie, self.device, "face reidentification", config.reident_model_xml, 2*self.QUEUE_SIZE, logger=logger)

        self.face_detector_out_blob_name = self.face_detector.get_out_blob_name()
        self.face_identifier_out_blob_name = self.face_identifier.get_out_blob_name()

        self.prev_rois = []
        self.prev_landmarks = []
        self.saved_prev_rois = []
        self.saved_prev_landmarks = []
        self.prev_feature_vectors = []
        self.prev_frame = None


    def process_frame(self, frame):

        orig_h, orig_w = frame.shape[:-1]
        frame = frame.transpose((2, 0, 1)) # HWC to CHW
        frame = np.expand_dims(frame, axis=0)

        # Do face detection inference
        self.face_detector.start_async([frame])

        if self.face_detector.wait_for_previous_batch(-1) == 0:
            # Get output bounding boxes
            detect_res = self.face_detector.get_raw_previous_outputs()[0][self.face_detector_out_blob_name]
            rois = []
            for i, data in enumerate(detect_res[0][0]):
                id, label, conf, x_min, y_min, x_max, y_max = data

                if conf < 0.1:
                    break # results are sorted by confidence decrease

                x_min = max(int(x_min * orig_w), 0)
                y_min = max(int(y_min * orig_h), 0)
                x_max = max(int(x_max * orig_w), 0)
                y_max = max(int(y_max * orig_h), 0)

                roi_width = x_max - x_min
                roi_height = y_max - y_min

                # We discard detection results that are too small, because it'll affect their reidentification accuracy
                if roi_width < self.min_roi_size or roi_height < self.min_roi_size:
                    continue

                roi = { "xmin": x_min, "ymin": y_min, "xmax": x_max, "ymax": y_max, "confidence": conf }
                rois.append(roi)

            # @Performance: Investigate what is happenning when rois length reaches QUEUE_SIZE,
            #               as performance notably drops

            landmarks = []
            if len(rois) > 0:
                # Rejecting requests that not fit into the queue
                if len(rois) > self.QUEUE_SIZE:
                    rois = rois[:self.QUEUE_SIZE]
                    log.warning("Some requests were rejected - exceeded request queue size: {}".format(self.QUEUE_SIZE))

                # Do landmarks stuff
                rois = rescale_rois(rois, self.roi_scale_factor)
                frame_crops = crop_rois_with_copy(self.prev_frame, rois)
                self.landmarks_detector.start_async(frame_crops)
                landmarks = self.landmarks_detector.get_outputs()
                landmarks = [landmark.reshape(-1, 2) for landmark in landmarks]

                # Do face identification inference
                self.align_crops(frame_crops, landmarks)
                self.face_identifier.start_async(frame_crops)

            feature_vectors = []
            if self.face_identifier.wait_for_previous_batch(-1) == 0:
                if len(self.prev_rois) > 0:
                    reident_res = self.face_identifier.get_raw_previous_outputs()

                    feature_vectors = [output[self.face_identifier_out_blob_name][0].reshape(1, -1) for output in reident_res]

            # Save feature vectors for next iteration when they will tracked (see note in the end of the function)
            self.prev_feature_vectors = feature_vectors

            # Save landmarks and rois for next iteration when they will be identified and then tracked
            self.saved_prev_landmarks = self.prev_landmarks
            self.saved_prev_rois = self.prev_rois
            self.prev_landmarks = landmarks
            self.prev_rois = rois

        self.prev_frame = frame

        # @Hack: Instead work with rois intertaly with np.array type
        converted_rois = [ np.array(list(roi.values())) for roi in self.saved_prev_rois ]

        detections = list()
        features = list()
        landmarks = list()
        for i, roi in enumerate(self.saved_prev_rois):
            # @Bug: Figure out why some feature_vector contains inf and how to deal with them properly
            if i >= len(self.prev_feature_vectors):
                feature_vector = np.ones((256,))
            else:
                feature_vector = self.prev_feature_vectors[i].reshape((256,))
            feature_vector = np.nan_to_num(feature_vector)
            features.append(feature_vector)

            box = [roi["xmin"], roi["ymin"], roi["xmax"], roi["ymax"], roi["confidence"]]
            detections.append(box)

            # @Bug: Figure out why i is exceeding array lenght. Same above
            if i >= len(self.prev_landmarks):
                landmarks.append(np.empty((5,2)))
            else:
                landmarks.append(self.prev_landmarks[i])

        detections = np.array(detections).reshape((-1, 5))
        features   = np.array(features).reshape((-1, 256))
        landmarks  = np.array(landmarks)
        
        online_targets = self.face_tracker.update(detections, features, landmarks, self.frame_size, self.frame_size)

        results = []
        for t in online_targets:
            tlwh = t.tlwh
            tlbr = t.tlbr
            tid = t.track_id
            vertical = tlwh[2] / tlwh[3] > 1.6
            if tlwh[2] * tlwh[3] > BYTE_Args().min_box_area and not vertical:
                bbox = dict(xmin=int(tlbr[0]), ymin=int(tlbr[1]), xmax=int(tlbr[2]), ymax=int(tlbr[3]))
                results.append(dict(id=tid, bbox=bbox, score=t.score))

        # @Note: We update tracker here, because it requires data from previous pipeline stages, which
        #        have been already executed on previous calls, and hence all data only becomes available
        #        at this time. We choose this behaviour, because update should be called on every iteration,
        #        even with no detections, in order to keep trackers age.
        # results = self.face_tracker.update(self.prev_feature_vectors, converted_rois, self.saved_prev_landmarks)
        # results = [{"bbox": tracker["bbox"], "id": tracker["id"]} for tracker in trackers]

        return results


    @staticmethod
    def normalize(array, axis):
        mean = array.mean(axis=axis)
        array -= mean
        std = array.std()
        array /= std
        return mean, std


    @staticmethod
    def get_transform(src, dst):
        assert np.array_equal(src.shape, dst.shape) and len(src.shape) == 2, \
            "2d input arrays are expected, got {} and {}".format(src.shape, dst.shape)
        src_col_mean, src_col_std = FaceRecognizer.normalize(src, axis=(0))
        dst_col_mean, dst_col_std = FaceRecognizer.normalize(dst, axis=(0))

        u, _, vt = np.linalg.svd(np.matmul(src.T, dst))
        r = np.matmul(u, vt).T

        transform = np.empty((2, 3))
        transform[:, 0:2] = r * (dst_col_std / src_col_std)
        transform[:, 2] = dst_col_mean.T - \
            np.matmul(transform[:, 0:2], src_col_mean.T)
        return transform


    def align_crops(self, face_images, face_landmarks):
        assert len(face_images) == len(face_landmarks), \
            "Input lengths differ, got %s and %s" % \
            (len(face_images), len(face_landmarks))

        for image, image_landmarks in zip(face_images, face_landmarks):
            assert len(image.shape) == 4, "Face image is expected"
            image = image[0]

            scale = np.array((image.shape[-1], image.shape[-2]))
            desired_landmarks = np.array(REFERENCE_LANDMARKS, dtype=np.float64) * scale
            landmarks = np.array(image_landmarks, dtype=np.float64) * scale

            transform = FaceRecognizer.get_transform(desired_landmarks, landmarks)
            img = image.transpose((1, 2, 0))
            cv.warpAffine(img, transform, tuple(scale), img,
                           flags=cv.WARP_INVERSE_MAP)
            image[:] = img.transpose((2, 0, 1))
