#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .face_recognition import FaceRecognizer, FaceRecognizerConfig
from .object_recognition import ObjectRecognizer, ObjectRecognizerConfig

from openvino.inference_engine import IECore

import pyzbar.pyzbar as pyzbar
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision

import os, sys, copy
import cv2 as cv
import numpy as np
from multiprocessing.pool import ThreadPool
import threading

import rospy
import rospkg

from std_msgs.msg import Header
from sweetie_bot_text_msgs.msg import DetectionArray as DetectionArrayMsg, Detection as DetectionMsg
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Pose, Point, Vector3, Quaternion
from std_msgs.msg import ColorRGBA

from tf.transformations import quaternion_from_matrix, rotation_matrix

from .common import intersection_over_union
from .common import draw_bbox, draw_qr, get_class_labels, draw_landmarks_on_image
from .hand_references_p3_cam import *

from .msg import FaceTrackerCommand as FaceTrackerCommandMsg

class Detection:
    __NO_ROTATION = Quaternion(1, 0, 0, 0)

    def __init__(self, id = None, label = None, type = None, pose = None, scale = None, attention_dir = None):
        # process id
        if id is None or isinstance(id, int):
            self.id = id
        else:
            raise TypeError("Detection: id must be int.")
        # process label
        if label is None or isinstance(label, str):
            self.label = label
        else:
            raise TypeError("Detection: label must be string.")
        # process type
        if not isinstance(type, str):
            raise TypeError("Detection: type must be string.")
        self.type = type

        self.pose = None
        # process pose
        if isinstance(pose, np.ndarray) and len(pose) == 3:
            self.pose = Pose(position=Point(x = pose[0], y = pose[1], z = pose[2]), orientation=Detection.__NO_ROTATION)
        elif isinstance(pose, Pose):
            self.pose = pose
        elif isinstance(pose, Point):
            self.pose = Pose(position=pose, orientation=Detection.__NO_ROTATION)
        else:
            raise TypeError("Detection: pose must be numpy.array, geometry_msgs/Point or geometry_msgs/Pose")
        # process scale
        if isinstance(scale, np.ndarray) and len(scale) == 3:
            self.scale = Point(x = scale[0], y = scale[1], z = scale[2])
        elif isinstance(scale, Point):
            self.scale = scale
        else:
            raise TypeError("Detection: scale must be numpy.array or geometry_msgs/Point")

        # process attention direction
        self.attention_dir = None
        if isinstance(attention_dir, np.ndarray) and len(attention_dir) == 3:
            self.attention_dir = Vector3(
                x = attention_dir[0],
                y = attention_dir[1],
                z = attention_dir[2],
            )

    def to_msg(self):
        msg = DetectionMsg()
        msg.id = self.id
        msg.label = self.label if self.label else ''
        msg.type = self.type
        msg.pose = self.pose
        return msg

    def to_attention_visualization_msg(self):
        if self.attention_dir is not None:
            msg = Marker()
            msg.ns = 'camera'
            msg.id = 4000+self.id
            msg.action = Marker.ARROW
            msg.scale = Vector3(x=0.01, y=0.03, z=0)

            length = 0.1
            direction = np.array([self.attention_dir.x, self.attention_dir.y, self.attention_dir.z])
            direction = direction / np.linalg.norm(direction)
            start_point = self.pose.position
            end_point = Point(
                start_point.x + direction[0] * length,
                start_point.y + direction[1] * length,
                start_point.z + direction[2] * length,
            )

            msg.points.append(start_point)
            msg.points.append(end_point)

            msg.color = ColorRGBA(r=1.0, g=1.0, b=1.0, a=0.6)
            msg.lifetime = rospy.Duration(1.0)
            msg.frame_locked = False

            return msg

    def to_visualizaton_msg(self):
        msg = Marker()
        msg.ns = 'camera'
        msg.id = self.id
        msg.action = Marker.ADD
        msg.scale = self.scale
        msg.pose = self.pose
        if self.type == 'face':
            msg.type = Marker.SPHERE
            #msg.scale = Vector3(x=0.15, y=0.15, z=0.15)
            msg.color = ColorRGBA(r=1.0, g=0.8, b=0.8, a=1.0)
        elif self.type == 'object':
            msg.type=Marker.CYLINDER
            #msg.scale = Vector3(x=0.05, y=0.05, z=0.05)
            msg.color = ColorRGBA(r=1.0, g=0.0, b=0.0, a=0.6)
            # Rotate cylinder by 90 degree around z axis
            # msg.pose.orientation = Quaternion(0.7071, 0, 0, 0.7071)
        elif self.type == 'qr':
            msg.type=Marker.CUBE
            #msg.scale = Vector3(x=0.05, y=0.05, z=0.05)
            msg.color = ColorRGBA(r=0.0, g=0.0, b=1.0, a=1.0)
        elif self.type == 'hand':
            msg.type=Marker.CYLINDER
            #msg.scale = Vector3(x=0.05, y=0.05, z=0.05)
            msg.color = ColorRGBA(r=1.0, g=1.0, b=1.0, a=0.6)
        msg.lifetime = rospy.Duration(1.0)
        msg.frame_locked = False
        return msg

    def to_visualizaton_text_msg(self):
        msg = Marker()
        msg.ns = 'camera'
        msg.id = 1000+self.id
        msg.action = Marker.ADD
        msg.type=Marker.TEXT_VIEW_FACING
        msg.scale.z = 0.05
        msg.color = ColorRGBA(r=1.0, g=1.0, b=1.0, a=1.0)
        msg.lifetime = rospy.Duration(1.0)
        msg.frame_locked = False

        text_shift = 0.2
        text_location = Point(self.pose.position.x, self.pose.position.y + text_shift, self.pose.position.z)
        msg.pose = Pose(position=text_location, orientation=Detection.__NO_ROTATION)
        if self.label:
            if self.type == "face":
                msg.text = "face_id: %s" % self.label
            elif self.type == "object":
                msg.text = "object: %s" % self.label
            elif self.type == "qr":
                msg.text = "qr_code: '%s'" % self.label
            elif self.type == "hand":
                msg.text = "hand_gesture: '%s'" % self.label
        else:
            msg.text = ''
        return msg

    def __repr__(self):
        return "(type: %s, id: %s, label: %s, pose: %s, scale: %s" % (self.type, self.id, self.label, self.pose, self.scale)


class VisionNode:
    # Currently supported: face, object, QR-code recognition and hand recognition
    RECOGNIZERS_COUNT = 4

    def __init__(self, cap):
        ie = IECore()

        self.cap = cap
        camera_width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
        camera_height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
        self.frame_size = (camera_width, camera_height)
        rospy.loginfo(f"Video stream attributes: WIDTH: {camera_width}, HEIGHT: {camera_height}")

        rospy.loginfo("Available computing devices: {}".format(ie.available_devices))

        self.thread_pool = ThreadPool(processes = VisionNode.RECOGNIZERS_COUNT)

        self.camera_matrix = get_checked_param("~camera_matrix", [ 311.32101388, 0, 323.01160557, 0, 311.4412557, 230.04057043, 0, 0, 1 ])
        self.camera_matrix = np.matrix(self.camera_matrix).reshape([3, 3])
        self.default_distance_to_camera = get_checked_param("~default_distance_to_camera", 0.3)

        self.apply_undistortion = get_checked_param("~apply_undistortion", False)
        self.distortion_coefficients = get_checked_param("~distortion_coefficients", [ -0.28924137, 0.07465516, -0.00109149, 0.00154906, -0.00780817 ])
        self.distortion_coefficients = np.array(self.distortion_coefficients)

        alpha = get_checked_param("~undistortion_alpha", 0.0, lambda a: 0 <= a and a <= 1, "Alpha must be between zero and one.")

        myriad_devices = []
        for device_name in ie.available_devices:
            if "MYRIAD" in device_name:
                myriad_devices.append(device_name)

        face_recognizer_device = get_checked_param("~face_recognizer/device", "AUTO").upper()
        object_recognizer_device = get_checked_param("~object_recognizer/device", "AUTO").upper()

        self.confirmed_gesture_label = dict(Right="undefined", Left="undefined")
        self.last_gesture_label = dict(Right="undefined", Left="undefined")
        self.gesture_repetition_count = dict(Right=0, Left=0)

        self.confirmed_index_direction = dict(Right=None, Left=None)

        # Auto selection of appropriate devices if multiple are present
        if len(myriad_devices) > 1:
            if face_recognizer_device == "AUTO":
                if object_recognizer_device != myriad_devices[0]:
                    face_recognizer_device = myriad_devices[0]
                else:
                    face_recognizer_device = myriad_devices[1]
                rospy.loginfo("Auto-selected device for face recognizer: {}".format(face_recognizer_device))

            if object_recognizer_device == "AUTO":
                if face_recognizer_device != myriad_devices[1]:
                    object_recognizer_device = myriad_devices[1]
                else:
                    object_recognizer_device = myriad_devices[0]
                rospy.loginfo("Auto-selected device for object recognizer: {}".format(object_recognizer_device))


        rospack = rospkg.RosPack()
        package_location = rospack.get_path("sweetie_bot_vision")

        face_recognizer_config = FaceRecognizerConfig(

            detection_model_xml = package_location + get_checked_param("~face_recognizer/detection_model_xml"),
            landmarks_model_xml = package_location + get_checked_param("~face_recognizer/landmarks_model_xml"),
            reident_model_xml = package_location + get_checked_param("~face_recognizer/reident_model_xml"),

            prob_threshold = get_checked_param("~face_recognizer/prob_threshold", 0.5),
            roi_scale_factor = get_checked_param("~face_recognizer/roi_scale_factor", 1.15),
            z_projection_scale = get_checked_param("~face_recognizer/z_projection_scale", 0.2),
            min_roi_size = get_checked_param("~face_recognizer/min_roi_size", 20),
            device = face_recognizer_device
        )

        object_recognizer_config = ObjectRecognizerConfig(

            yolo_model_xml = package_location + get_checked_param("~object_recognizer/yolo_model_xml"),

            prob_threshold = get_checked_param("~object_recognizer/prob_threshold", 0.5),
            iou_threshold = get_checked_param("~object_recognizer/iou_threshold", 0.4),
            device = object_recognizer_device
        )

        self.hand_z_projection_scale = get_checked_param("~hand_recognizer/z_projection_scale", 0.17)
        self.qr_z_projection_scale = get_checked_param("~qr_recognizer/z_projection_scale", 0.15)


        self.face_recognizer   = FaceRecognizer(ie, face_recognizer_config, self.frame_size, logger=rospy.loginfo, errlogger=rospy.logerr)
        self.object_recognizer = ObjectRecognizer(ie, object_recognizer_config, logger=rospy.loginfo)

        options = vision.HandLandmarkerOptions(
            base_options=python.BaseOptions(model_asset_path=package_location+'/config/hand_landmarker.task'),
            num_hands = 2,
            # min_hand_detection_confidence = 0.3,
            running_mode = vision.RunningMode.IMAGE,
            #result_callback = self.hand_landmarks_callback
        )
        self.hand_detector = vision.HandLandmarker.create_from_options(options)
        self.hand_landmarks_ready_event = threading.Event()

        labels_file = os.path.splitext(object_recognizer_config.yolo_model_xml)[0] + ".labels"
        self.object_labels = get_class_labels(labels_file, rospy.logwarn)

        rospy.loginfo("Warming up stages of neural models pipeline")
        self.first_frame = self.second_frame = self.third_frame = None
        self.warm_up_recognition_pipeline(cap)

        size = (camera_width, camera_height)
        # Refining the camera matrix using parameters obtained by calibration
        # @Note: centerPrincipalPoint flag is important here, because it tells opencv
        #        not to stretch image on x and y axis after transformation
        self.optimal_camera_matrix, roi = cv.getOptimalNewCameraMatrix(self.camera_matrix, self.distortion_coefficients, size, alpha, size, centerPrincipalPoint = True)
        self.mapx, self.mapy = cv.initUndistortRectifyMap(self.camera_matrix, self.distortion_coefficients, None, self.optimal_camera_matrix, size, 5)

    def __del__(self):
        try:
            self.thread_pool.close()
        except:
            self.thread_pool.terminate()
        finally:
            self.thread_pool.join()

    def handle_face_tracker_command(self, request):
        if request.command   == "freeze":
            rospy.loginfo(f"COMMAND: Freezing features of face with id {request.face_id}")
            self.face_recognizer.face_tracker.change_track_freeze_state(request.face_id, True)
        elif request.command == "unfreeze":
            rospy.loginfo(f"COMMAND: Unreezing features of face with id {request.face_id}")
            self.face_recognizer.face_tracker.change_track_freeze_state(request.face_id, False)
        elif request.command == "set_freeze":
            rospy.loginfo(f"COMMAND: Setting features freeze state of face with id {request.face_id} to {request.freeze_state}")
            self.face_recognizer.face_tracker.change_track_freeze_state(request.face_id, request.freeze_state)
        elif request.command == "swap_ids":
            if request.second_face_id != -1:
                rospy.loginfo(f"COMMAND: Swapping ids of faces {request.face_id} and {request.second_face_id}")
                self.face_recognizer.face_tracker.swap_track_ids(request.face_id, request.second_face_id)
        elif request.command == "set_id":
            self.face_recognizer.face_tracker.set_track_id(request.face_id, request.second_face_id)
            rospy.loginfo(f"COMMAND: Assigning id {request.second_face_id} to face {request.face_id}")
        elif request.command == "renew_id":
            rospy.loginfo(f"COMMAND: Assigning new id to face {request.face_id}")
            self.face_recognizer.face_tracker.renew_track_id(request.face_id)
        else:
            rospy.logerr(f"Received unknown command {request.command}")

    def warm_up_recognition_pipeline(self, cap):
        # Filling up the first two stages of pipeline
        ret, self.first_frame = cap.read()
        if not ret:
           rospy.logerr("Failed to read the first frame from the stream")
           sys.exit(1)

        self.face_recognizer.process_frame(self.first_frame)
        self.object_recognizer.process_frame(self.first_frame)

        ret, self.second_frame = cap.read()
        if not ret:
           rospy.logerr("Failed to read the second frame from the stream")
           sys.exit(1)

        self.face_recognizer.process_frame(self.second_frame)

        self.third_frame = None

    def hand_landmarks_callback(self, landmarks):
        self.hand_landmarks_ready_event.set()
        self.landmarks_results = landmarks

    def detect_hands(self, frame):
        frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        input_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=frame)
        landmarks_results = self.hand_detector.detect(input_image)
        # self.hand_landmarks_ready_event.wait()
        return landmarks_results

    def process_next_frame(self, show_video_result = False):
        # Read and process input frames
        success, frame = self.cap.read()

        if self.apply_undistortion:
            self.third_frame = cv.remap(frame, self.mapx, self.mapy, cv.INTER_LINEAR)
        else:
            self.third_frame = frame

        if not success:
            rospy.logwarn("Failed to grab next frame")
            return None

        # Run recognizers in separate threads
        face_detector_thread   = self.thread_pool.apply_async(self.face_recognizer.process_frame,   (self.third_frame,))
        # object_detector_thread = self.thread_pool.apply_async(self.object_recognizer.process_frame, (self.second_frame,))
        qrcode_detector_thread = self.thread_pool.apply_async(pyzbar.decode,                        (self.first_frame,))
        hand_detector_thread   = self.thread_pool.apply_async(self.detect_hands,                    (self.first_frame,))

        # Get results
        detections = []

        face_detections   = face_detector_thread.get()
        object_detections = []#object_detector_thread.get()

        VisionNode.filter_faces_overlaping_with_plushies(face_detections, object_detections)

        postprocessed_face_detections   = self.postprocess_results(face_detections, "face", frame.shape)
        postprocessed_object_detections = self.postprocess_results(object_detections, "object", frame.shape)
        detections.extend(postprocessed_face_detections)
        detections.extend(postprocessed_object_detections)

        qrcode_detections = qrcode_detector_thread.get()
        postprocessed_qrcode_detections = self.postprocess_results(qrcode_detections, "qr", frame.shape)
        detections.extend(postprocessed_qrcode_detections)

        hand_detections = hand_detector_thread.get()
        if hand_detections.hand_landmarks:
            hand_detections_list = list(zip(hand_detections.hand_landmarks, hand_detections.handedness))
        else:
            hand_detections_list = []
        postprocessed_hand_detections = self.postprocess_results(hand_detections_list, "hand", frame.shape)
        detections.extend(postprocessed_hand_detections)

        if show_video_result:
            # Draw hand detections
            if hand_detections.hand_landmarks:
                draw_landmarks_on_image(self.first_frame, hand_detections)

            # Draw predicted gesture
            put_text = lambda t, p: cv.putText(self.first_frame, t, p, cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv.LINE_AA)
            if self.confirmed_gesture_label["Right"] != "undefined" and \
               self.confirmed_gesture_label["Left"] != "undefined":
                put_text("Left hand: " + self.confirmed_gesture_label["Right"], (10, 20))
                put_text("Righ hand: " + self.confirmed_gesture_label["Left"],  (10, 40))
            elif self.confirmed_gesture_label["Right"] != "undefined":
                put_text("Left hand: " + self.confirmed_gesture_label["Right"], (10, 20))
            elif self.confirmed_gesture_label["Left"] != "undefined":
                put_text("Righ hand: " + self.confirmed_gesture_label["Left"],  (10, 20))

            # Draw face detections
            for face_detection in face_detections:
                draw_bbox(self.first_frame, face_detection, "id",
                          lambda id: "id="+str('Unknown' if id == None else id), label_on_inside = True)

            # Draw object detections
            for object_detection in object_detections:
                draw_bbox(self.first_frame, object_detection, "class_id",
                                     lambda id: "object_id="+str(id) if self.object_labels is None else str(self.object_labels[id]))
            # Draw QR-codes detections
            for qrcode_detection in qrcode_detections:
                draw_qr(self.first_frame, qrcode_detection)

            # Display the resulting frame
            cv.imshow('floating', self.first_frame)
            cv.waitKey(1)


        # Move pipeline one step further
        self.first_frame = self.second_frame
        self.second_frame = self.third_frame

        return detections

    @staticmethod
    def filter_faces_overlaping_with_plushies(face_detections, object_detections, throw_away_threshold = 0.4):
        if len(face_detections) == 0:
            return

        # Throwing away face detections that overlap with 0 class objects (plushie_face)
        for object in object_detections:
            if object["class_id"] == 0:
                for i, face in enumerate(face_detections):
                    if intersection_over_union(object["bbox"], face["bbox"]) > throw_away_threshold:
                        del face_detections[i]


    def postprocess_results(self, results, result_type, frame_shape):
        if not isinstance(results, list):
            assert False, "results must be a list, got a %s" % type(results)

        # Reset filtered hand state, if there's no more detections
        if len(results) == 0 and result_type == "hand":
            # Filtering of hand pose detections in case of undefined gestures
            for handedness_label in self.last_gesture_label.keys():
                last_gesture_label = self.last_gesture_label[handedness_label]
                confirmed_gesture_label = self.confirmed_gesture_label[handedness_label]

                if last_gesture_label == "undefined":
                    self.gesture_repetition_count[handedness_label] += 1
                else:
                    self.gesture_repetition_count[handedness_label] = 0

                if self.gesture_repetition_count[handedness_label] >= 3:
                    self.confirmed_gesture_label[handedness_label] = "undefined"

            self.last_gesture_label = dict(Right="undefined", Left="undefined")

        if len(results) == 0:
            return []

        frame_width = frame_shape[0]
        frame_height = frame_shape[1]

        postprocessed_results = []
        # TODO: Figure out better solution
        # Offset to avoiding marker ids intersection for different types of detected objects
        ID_OFFSET = 0 if result_type == "face" else 10000 if result_type == "object" else 20000
        for i, result in enumerate(results):
            if result_type == "face" or result_type == "object":
                box = result["bbox"]
                cx = (box["xmin"] + box["xmax"]) / 2.0
                cy = (box["ymin"] + box["ymax"]) / 2.0

                w = (box["xmax"] - box["xmin"]) / frame_width
                h = (box["ymax"] - box["ymin"]) / frame_height

            elif result_type == "hand":
                (hand_landmarks, handedness) = result

                new_gesture_label = "undefined"
                handedness_label = handedness[0].category_name

                hand_greeting_threshold = 0.014
                brohoove_threshold = 0.035
                # @Cleanup
                # pointing_index_threshold = 0.06
                # finger_open_threshold = 0.02

                open_angle_threshold = 1.3
                def recognize_gesture(hand_greeting_reference, brohoove_reference):
                    aligned_hand_arr = self.align_hand(hand_landmarks, hand_greeting_reference)
                    aligned_brohoove_arr = self.align_hand(hand_landmarks, brohoove_reference)

                    hand_greeting_error = VisionNode.hand_landmarks_error(aligned_hand_arr, hand_greeting_reference)
                    brohoove_error = VisionNode.hand_landmarks_error(aligned_brohoove_arr, brohoove_reference)

                    # Determining if index finger is open
                    index_connections = vision.HandLandmarksConnections.HAND_INDEX_FINGER_CONNECTIONS
                    index_error = VisionNode.finger_landmarks_error(aligned_hand_arr, hand_greeting_reference, index_connections)
                    index_angle = VisionNode.finger_angle(aligned_hand_arr, index_connections)

                    middle_connections = vision.HandLandmarksConnections.HAND_MIDDLE_FINGER_CONNECTIONS
                    middle_error = VisionNode.finger_landmarks_error(aligned_hand_arr, hand_greeting_reference, middle_connections)
                    middle_angle = VisionNode.finger_angle(aligned_hand_arr, middle_connections)

                    ring_connections = vision.HandLandmarksConnections.HAND_RING_FINGER_CONNECTIONS
                    ring_error = VisionNode.finger_landmarks_error(aligned_hand_arr, hand_greeting_reference, ring_connections)

                    ring_angle = VisionNode.finger_angle(aligned_hand_arr, ring_connections)

                    pinky_connections = vision.HandLandmarksConnections.HAND_PINKY_FINGER_CONNECTIONS
                    pinky_error = VisionNode.finger_landmarks_error(aligned_hand_arr, hand_greeting_reference, pinky_connections)

                    pinky_angle = VisionNode.finger_angle(aligned_hand_arr, pinky_connections)

                    thumb_connections = vision.HandLandmarksConnections.HAND_THUMB_CONNECTIONS
                    thumb_angle = VisionNode.finger_angle(aligned_hand_arr, thumb_connections)

                    v_condition = index_angle < open_angle_threshold and \
                                  middle_angle < open_angle_threshold and \
                                  (ring_angle > open_angle_threshold or \
                                  pinky_angle > open_angle_threshold)

                    pointing_index_condition = index_angle < open_angle_threshold and \
                                               (middle_angle > open_angle_threshold or \
                                               ring_angle > open_angle_threshold or \
                                               pinky_angle > open_angle_threshold)

                    # @Cleanup
                    # pointing_index_condition = index_error < pointing_index_threshold and \
                    #                            (middle_error > finger_open_threshold or \
                    #                            ring_error > finger_open_threshold or \
                    #                            pinky_error > finger_open_threshold)

                    # v_condition = index_error < pointing_index_threshold and \
                    #               middle_error < pointing_index_threshold and \
                    #               (ring_error > finger_open_threshold or \
                    #               pinky_error > finger_open_threshold)

                    if v_condition:
                        return "victory"
                    elif pointing_index_condition:
                        return "pointing_index"
                    else:
                        if hand_greeting_error < hand_greeting_threshold:
                            return "hand_greeting"
                        elif brohoove_error < brohoove_threshold:
                            return "brohoove"

                    return "undefined"

                # @Note: Hands are reversed, because of mirroring inside detectors
                if handedness[0].category_name == "Right":
                    new_gesture_label = recognize_gesture(HAND_GREETING_LEFT_REFERENCE, BROHOOVE_LEFT_REFERENCE)
                else:
                    new_gesture_label = recognize_gesture(HAND_GREETING_RIGHT_REFERENCE, BROHOOVE_RIGHT_REFERENCE)


                # Set for hand position orthogonal projection of first visible point
                # Better if instead it will the center of hand's bounding box
                cx = hand_landmarks[0].x * self.frame_size[0]
                cy = hand_landmarks[0].y * self.frame_size[1]

                # get size
                y = [landmark.y * self.frame_size[0]  for landmark in hand_landmarks]
                h = (max(y) - min(y)) / frame_height

            elif result_type == "qr":
                # Check if zbar outputed what's we need
                if result.type != "QRCODE":
                    continue

                points = result.polygon
                w = result.rect.width / frame_width
                h = result.rect.height / frame_height

                c = np.array([points[0].x, points[0].y], dtype=np.float32)
                for i in range(1, len(points)):
                    c += points[i]
                c /= len(points)

                cx, cy = c[0], c[1]

            # Approximate z coordinates of object impericaly from its size
            if result_type == "face":
                z = self.face_recognizer.config.z_projection_scale / h
            elif result_type == "hand":
                z = self.qr_z_projection_scale / h
            elif result_type == "qr":
                z = self.hand_z_projection_scale / h
            else:
                # For object with big range of possible sizes, assume its location on constant plane
                z = self.default_distance_to_camera


            attention_direction = None
            pose = VisionNode.back_projection(cx, cy, z, self.optimal_camera_matrix)


            size_scale = 0.3 # TODO: Think about what the right way to convert size of bbox to scale of marker
            filter_weight = 0.3
            if result_type == "face":
                label = str(result["id"])

                size = size_scale * np.array([w, h, 0.15])

            elif result_type == "object":
                if self.object_labels is None:
                    label = str(result["class_id"])
                else:
                    label = self.object_labels[result["class_id"]]

                size = size_scale * np.array([w, h, 0.15])

            elif result_type == "qr":
                size = size_scale * np.array([0.15, 0.15, 0.15])

                if b'gosuslugi.ru/vaccine' in result.data or \
                   b"immune.mos.ru" in result.data:
                    label = "qr_vaccine_code"
                else:
                    label = result.data.decode("utf-8")
            elif result_type == "hand":
                size = size_scale * np.array([0.15, 0.15, 0.15])

                # Filtering of hand pose detections
                last_gesture_label = self.last_gesture_label[handedness_label]

                if last_gesture_label == new_gesture_label:
                    self.gesture_repetition_count[handedness_label] += 1
                else:
                    self.gesture_repetition_count[handedness_label] = 0

                label = self.confirmed_gesture_label[handedness_label]
                if self.gesture_repetition_count[handedness_label] >= 3:
                    label = new_gesture_label
                    self.confirmed_gesture_label[handedness_label] = new_gesture_label

                if self.confirmed_gesture_label[handedness_label] == "pointing_index":
                    dir_x = hand_landmarks[6].x - hand_landmarks[5].x
                    dir_y = hand_landmarks[6].y - hand_landmarks[5].y
                    dir_z = hand_landmarks[6].z - hand_landmarks[5].z

                    attention_direction = np.array([-dir_x, -dir_y, dir_z])
                    self.confirmed_index_direction[handedness_label] = attention_direction

                self.last_gesture_label[handedness_label] = new_gesture_label
            else:
                assert False, "Cannot postprocess detection results. Wrong object type"

            detection = Detection(type = result_type, id = ID_OFFSET + i, label = label, pose = pose, scale = size, attention_dir = attention_direction)
            postprocessed_results.append(detection)

        return postprocessed_results

    @staticmethod
    def finger_angle(prediction, finger_connections):
        N = len(finger_connections) + 1 # +1 for firts MCP point of finger

        wrist_index = 0
        finger_start_index = finger_connections[0].start
        wrist_finger_vec = np.array(prediction[finger_start_index]) - np.array(prediction[wrist_index])

        accumulated_angle = 0
        finger_vectors = [wrist_finger_vec]
        for connection in finger_connections:

            prev_vec = finger_vectors[-1]
            curr_vec = np.array(prediction[connection.end]) - np.array(prediction[connection.start])

            normalization = np.sqrt(prev_vec.dot(prev_vec)) * np.sqrt(curr_vec.dot(curr_vec))
            accumulated_angle += np.arccos(np.dot(curr_vec, prev_vec) / normalization)

            finger_vectors.append(curr_vec)

        return accumulated_angle

    @staticmethod
    def finger_landmarks_error(groundtruth, prediction, finger_connections):
        assert np.size(groundtruth) == np.size(prediction), "Array sizes not equal"
        N = len(finger_connections) + 1 # +1 for firts MCP point of finger

        finger_mcp_index = finger_connections[0].start
        sum = np.linalg.norm(np.array(groundtruth[finger_mcp_index]) - np.array(prediction[finger_mcp_index]))

        for connection in finger_connections:
            finger_index = connection.end
            sum += np.linalg.norm(np.array(groundtruth[finger_index]) - np.array(prediction[finger_index]))

        return sum / N

    @staticmethod
    def hand_landmarks_error(groundtruth, prediction):
        assert np.size(groundtruth) == np.size(prediction), "Array sizes not equal"
        N = len(groundtruth)

        sum = np.linalg.norm(np.array(groundtruth[0]) - np.array(prediction[0]))

        # @Note: Ignoring indices from 1 to 4, because we exclude thumb from calculations
        for i in range(5, N):
            sum += np.linalg.norm(np.array(groundtruth[i]) - np.array(prediction[i]))

        return sum / N

    @staticmethod
    def back_projection(cx, cy, z, camera_matrix):
        # convert to image center relative position
        cx -= camera_matrix[0,2]
        cy -= camera_matrix[1,2]
        # calculate object position
        x = - z / camera_matrix[0,0] * cx
        y = - z / camera_matrix[1,1] * cy

        return np.array([x, y, z])

    def align_hand(self, detected_landmarks, reference_landmarks):
        if detected_landmarks is None:
            return np.empty((0,3))

        assert len(detected_landmarks) == len(reference_landmarks), \
            "Input lengths differ, got %s and %s" % \
            (len(detected_landmarks), len(reference_landmarks))

        rows, cols = self.frame_size
        depth = self.default_distance_to_camera * 100

        x = [landmark.x * cols  for landmark in detected_landmarks]
        y = [landmark.y * rows  for landmark in detected_landmarks]
        z = [landmark.z * depth for landmark in detected_landmarks]
        hand_coordinates = np.column_stack((x, y, z))

        desired_landmarks = np.array(reference_landmarks, dtype=np.float64)
        landmarks = np.array(hand_coordinates, dtype=np.float64)

        # Denormalized reference landmarks
        desired_landmarks[:, 0] *= cols
        desired_landmarks[:, 1] *= rows
        desired_landmarks[:, 2] *= depth

        success, transform, inliers = cv.estimateAffine3D(landmarks, desired_landmarks)

        fourth_row = np.array([[0, 0, 0, 1]])
        transform4 = np.row_stack((transform, fourth_row))

        transformed_landmarks = cv.perspectiveTransform(landmarks.reshape(-1, 1, 3), transform4)
        transformed_landmarks = transformed_landmarks.reshape(-1, 3)

        # Denormalized resulting landmarks
        transformed_landmarks[:, 0] *= 1./cols
        transformed_landmarks[:, 1] *= 1./rows
        transformed_landmarks[:, 2] *= 1./depth

        return transformed_landmarks


def get_checked_param(param_name, default_value = None, predicate = None, fail_message = None, override_predicate = None, override_fail_message = None):
    if default_value is not None:
        param_value = rospy.get_param(param_name, default_value)
    else:
        try:
            param_value = rospy.get_param(param_name)
        except:
            rospy.logerr('"%s" mandatory parameter is not defined.' % (param_name))
            sys.exit(-1)
    if override_predicate is None:
        if default_value is not None and not isinstance(param_value, type(default_value)):
            param_name = param_name[1:] if param_name[0] == "~" else param_name
            rospy.logerr('"%s" parameter must have %s type.' % (param_name, type(default_value).__name__))
            sys.exit(-1)
    elif not override_predicate(param_value):
        if not override_fail_message is None:
            rospy.logerr(override_fail_message)
        sys.exit(-1)

    if not predicate is None and not predicate(param_value):
        if not fail_message is None:
            rospy.logerr(fail_message)
        sys.exit(-1)


    return param_value


def main():
    rospy.loginfo("Initializing...")

    rospy.init_node("sweetie_bot_vision")

    # Parameters
    camera_frame = get_checked_param("~camera_frame", "camera_link_optical")
    fps_lock = get_checked_param("~fps_lock", 15)
    input_stream = get_checked_param("~input_stream", -1, override_predicate = lambda p: isinstance(p, int) or isinstance(p, str), override_fail_message = '"~input_stream" must be an int or a string')
    show_video_result = get_checked_param("~show_video_result", False)

    # @Hack: It's not a great way to identify gstreamer by udpsrc/tcpclientsrc element names, but as long
    #        as we're not using anything except those elements, that will do.
    is_gstreamer_input = isinstance(input_stream, str) and (input_stream.split()[0] == "udpsrc" or input_stream.split()[0] == "tcpclientsrc")
    if is_gstreamer_input:
        rospy.loginfo("Opening gstreamer input stream: " + str(input_stream))
        cap = cv.VideoCapture(input_stream, cv.CAP_GSTREAMER)
    else:
        rospy.loginfo("Opening input stream: " + str(input_stream))
        cap = cv.VideoCapture(input_stream)

    if not cap.isOpened():
        rospy.logerr("Cannot open video stream")
        sys.exit(1)

    # We can try to set FPS only on local computer camera
    if input_stream == -1:
        cap.set(cv.CAP_PROP_FPS, fps_lock)

    if show_video_result:
        cv.namedWindow('floating', cv.WND_PROP_FULLSCREEN)
        cv.setWindowProperty('floating', cv.WND_PROP_AUTOSIZE, cv.WINDOW_AUTOSIZE)

    vision_node = VisionNode(cap)

    face_tracker_sub = rospy.Subscriber("face_tracker_command", FaceTrackerCommandMsg, vision_node.handle_face_tracker_command)

    detections_pub = rospy.Publisher("detections", DetectionArrayMsg, queue_size=5)
    markers_pub = rospy.Publisher("hmi/detections", MarkerArray, queue_size=5)

    rospy.loginfo("Vision node running")

    while not rospy.is_shutdown() and cap.isOpened():

        # @Note: Stamp must been taken along with frame capture, and because
        #        process function calls to frame read first, we take timestamp here.
        stamp = rospy.Time.now()

        detections = vision_node.process_next_frame(show_video_result)

        if detections is None:
            break

        # Create DetectionArray message
        detections_msg = DetectionArrayMsg()
        detections_msg_header = Header(stamp = stamp, frame_id = camera_frame)
        for detection in detections:
            detections_msg.detections.append(detection.to_msg())
            last = detections_msg.detections[-1]
            # @Hack: remap types
            if last.type == 'face':
                last.type = 'human'
            # set header
            last.header = detections_msg_header

        # Create MarkerArray
        markers_msg = MarkerArray()
        for detection in detections:
            marker_msg = detection.to_visualizaton_msg()
            marker_msg.header = Header(stamp = stamp, frame_id = camera_frame)
            markers_msg.markers.append(marker_msg)

            marker_label_msg = detection.to_visualizaton_text_msg()
            marker_label_msg.header = Header(stamp = stamp, frame_id = camera_frame)
            markers_msg.markers.append(marker_label_msg)

            attent_msg = detection.to_attention_visualization_msg()
            if attent_msg is not None:
                attent_msg.header = Header(stamp = stamp, frame_id = camera_frame)
                markers_msg.markers.append(attent_msg)

        # Publish both
        if len(detections) > 0:
            detections_pub.publish(detections_msg)
            markers_pub.publish(markers_msg)


    rospy.loginfo("Vision node: Node shutted down")

    cap.release()
