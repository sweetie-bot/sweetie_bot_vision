#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, datetime
import cv2 as cv
import numpy as np
import logging as log
from multiprocessing.pool import ThreadPool

from .face_recognition import FaceRecognizer, FaceRecognizerConfig
from .object_recognition import ObjectRecognizer, ObjectRecognizerConfig
from .common import draw_bbox, draw_qr

from openvino.inference_engine import IECore
import pyzbar.pyzbar as pyzbar

import rospkg

# Params
fps_lock = 30
record_output = False

#
# For gstreamer mode we're assume that sender side runs following command:
#
# gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,width=640,height=480,framerate=30/1 ! videoconvert ! jpegenc !  rtpjpegpay !  udpsink host=<reciever-ip> port=5000
#

# Whereas this reciever script capturing stream with parameters:
gstreamer_str = 'udpsrc port=5000 ! application/x-rtp, encoding-name=JPEG,payload=26 !  rtpjpegdepay !  jpegdec ! videoconvert ! appsink'

# Params parse
if len(sys.argv) > 1:
    if sys.argv[1] == "--help" or sys.argv[1] == "-h" or sys.argv[1] == "-help":
        print("Usage: rosrun sweetie_bot_vision demo.py [cam|gstreamer|<path/to/a/video.mp4>] [AUTO|CPU|MYRIAD]")
        exit(0)

    if sys.argv[1] == "gstreamer":
        input_stream = gstreamer_str
    elif sys.argv[1] == "cam":
        input_stream = -1
    else:
        input_stream = sys.argv[1]
else:
    input_stream = -1

if len(sys.argv) > 2:
    device = sys.argv[2]
else:
    device = "AUTO"

rospack = rospkg.RosPack()
package_location = rospack.get_path("sweetie_bot_vision")

log.basicConfig(format="[ %(levelname)s ] %(message)s", level=log.INFO, stream=sys.stdout)

#
# Models configurations
#
face_recognizer_config = FaceRecognizerConfig(

    detection_model_xml = package_location + "/models/face_detection/FP16/face-detection-retail-0004.xml",
    reident_model_xml = package_location + "/models/face_reidentification/FP16/face-reidentification-retail-0095.xml",
    landmarks_model_xml = package_location + "/models/landmarks_regression/FP16/landmarks-regression-retail-0009.xml",

    prob_threshold = 0.5,
    roi_scale_factor = 1.15,
    min_roi_size = 20,
    device = device
)

object_recognizer_config = ObjectRecognizerConfig(

    yolo_model_xml = package_location + "/models/object_recognition/FP16/yolov4-tiny.xml",

    prob_threshold = 0.25,
    iou_threshold = 0.4,
    device = device
)

def get_class_labels(labels_file):
    try:
        with open(labels_file, 'r') as f:
            labels = [ label for label in f.read().split('\n') if label != "" ]

            return labels

    except (IOError) as e:
        log.warn("Cannot open object labels file. Displaying raw class ids instead")

        return None

def main():
    if input_stream == gstreamer_str:
        cap = cv.VideoCapture(input_stream, cv.CAP_GSTREAMER)
    else:
        cap = cv.VideoCapture(input_stream)

    if not cap.isOpened():
        log.error("Cannot open video stream")
        exit(1)

    ie = IECore()

    face_recognizer = FaceRecognizer(ie, face_recognizer_config)
    object_recognizer = ObjectRecognizer(ie, object_recognizer_config)

    labels_file = os.path.splitext(object_recognizer_config.yolo_model_xml)[0] + ".labels"
    object_class_labels = get_class_labels(labels_file)

    # Change device name for correct name displaying
    global device
    if device == "AUTO":
        if "MYRIAD" in ie.available_devices:
            device = "MYRIAD"
        else:
            device = "CPU"
    else:
        device = device

    # We can try to set FPS only on local computer camera
    if input_stream == -1:
        cap.set(cv.CAP_PROP_FPS, fps_lock)

    number_input_frames = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    number_input_frames = 1 if number_input_frames != -1 and number_input_frames < 0 else number_input_frames

    out = None
    if record_output:
        fourcc = cv.VideoWriter_fourcc(*'XVID')
        width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
        out = cv.VideoWriter('./vision_demo_output.avi', fourcc, 20.0, (width, height))

    log.info("Start processing video stream. Number of input frames: {}".format(number_input_frames))

    # Number of frames in picture is 1 and this will be read in cycle. Sync mode is default value for this case
    frame = None
    next_frame = None
    if number_input_frames != 1:
        # For async mode make first two iteration
        _, frame = cap.read()
        assert not frame is None, "frame is None"
        face_recognizer.process_frame(frame)
        object_recognizer.process_frame(frame)

        _, next_frame = cap.read()
        assert not frame is None, "next_frame is None"
        face_recognizer.process_frame(next_frame)

    detector_functions = [face_recognizer.process_frame, object_recognizer.process_frame, pyzbar.decode]
    drawing_functions  = [lambda f, d: draw_bbox(f, d, "id",
                                                 lambda id: "face_id="+str('Unknown' if id == None else id)),
                          lambda f, d: draw_bbox(f, d, "class_id",
                                                 lambda id: "object_id="+str(id) if object_class_labels is None else str(object_class_labels[id])),
                          draw_qr]

    assert len(detector_functions) == len(drawing_functions)

    # Variables for FPS counting
    start_time = datetime.datetime.now()
    fps_output_rate = 1
    frame_counter = 0
    fps_value = cap.get(cv.CAP_PROP_FPS) # Approximate fps value
    min_fps = 100
    fps_sum = 0
    total_frames_counter = 1

    thread_pool = ThreadPool(processes = len(detector_functions))

    while cap.isOpened():

        iteration_start_time = datetime.datetime.now()

        # Read and process input frames
        success, next_second_frame = cap.read()

        if not success:
            break

        detector_inputs = [next_second_frame, next_frame, frame]
        detectors = zip(detector_functions, detector_inputs)
    
        threads = []
        for function, frame in detectors:
            thread = thread_pool.apply_async(function, (frame,))
            threads.append(thread)

        for i, thread in enumerate(threads):
            detections = thread.get()

            for result in detections:
                if not result is None:
                    drawing_functions[i](frame, result)

        iteration_end_time = datetime.datetime.now()
        iteration_time = iteration_end_time - iteration_start_time 
        log.info("Iteration time: {} ms".format(iteration_time.total_seconds() * 1000))

        # Calculate fps
        frame_counter += 1
        if (datetime.datetime.now() - start_time).total_seconds() > fps_output_rate:
            fps_value = frame_counter / (datetime.datetime.now() - start_time).total_seconds()
            frame_counter = 0
            start_time = datetime.datetime.now()

        if fps_value < min_fps:
            min_fps = fps_value
        total_frames_counter += 1
        fps_sum += fps_value
        cv.putText(frame, 'FPS: {:.1f}, device: {}'.format(fps_value, device), (10, 25), cv.FONT_HERSHEY_SIMPLEX, 0.75, 0, 1, cv.LINE_AA)

        if record_output:
            out.write(frame)

        # Display the resulting frame
        cv.imshow('floating', frame)

        # We look two frames ahead
        frame = next_frame
        next_frame = next_second_frame

        if cv.waitKey(1) & 0xff == ord('q'):
            break


    thread_pool.terminate()
    thread_pool.join()

    log.info("Average FPS: {}".format(fps_sum / total_frames_counter))
    log.info("Min FPS: {}".format(min_fps))

    # When everything done, release the capture
    cap.release()
    if record_output:
        out.release()
        log.info("Video has been written into vision_demo_output.avi")
    cv.destroyAllWindows()
