
import numpy as np
import logging as log
from math import exp as exp

from typing import NamedTuple
from time import time

from openvino.inference_engine import IECore
import ngraph as ng

from .model import Model
from .common import resize_frame, intersection_over_union

class ObjectRecognizerConfig(NamedTuple):

    yolo_model_xml : str

    prob_threshold : float
    iou_threshold : float
    device : str

class YoloParams:
    # ------------------------------------------- Extracting layer parameters ------------------------------------------
    # Magic numbers are copied from yolo samples
    def __init__(self, param, side):
        self.num = 3 if 'num' not in param else param['num']
        self.coords = 4 if 'coords' not in param else param['coords']
        self.classes = 80 if 'classes' not in param else param['classes']
        self.side = side
        self.anchors = [10.0, 13.0, 16.0, 30.0, 33.0, 23.0, 30.0, 61.0, 62.0, 45.0, 59.0, 119.0, 116.0, 90.0, 156.0,
                        198.0, 373.0, 326.0] if 'anchors' not in param else param['anchors']

        self.isYoloV3 = False

        if param.get('mask'):
            mask = param['mask']
            self.num = len(mask)

            maskedAnchors = []
            for idx in mask:
                maskedAnchors += [self.anchors[idx * 2], self.anchors[idx * 2 + 1]]
            self.anchors = maskedAnchors

            self.isYoloV3 = True # Weak way to determine but the only one.

    def log_params(self):
        params_to_print = {'classes': self.classes, 'num': self.num, 'coords': self.coords, 'anchors': self.anchors}
        [log.info("         {:8}: {}".format(param_name, param)) for param_name, param in params_to_print.items()]


class ObjectRecognizer:
    QUEUE_SIZE = 16

    def __init__(self, inference_engine, config, logger=log.info):

        self.ie = inference_engine
        self.logger = logger

        self.device = config.device
        self.prob_threshold = config.prob_threshold
        self.iou_threshold = config.iou_threshold

        assert config.yolo_model_xml != "", "You're not specified path to YOLO model"

        self.yolo_model = Model(self.ie, self.device, "yolo", config.yolo_model_xml, self.QUEUE_SIZE, logger=logger)
        self.net = self.yolo_model.get_net()

        net_ops = ng.function_from_cnn(self.net).get_ordered_ops()
        self.net_layers = {op.friendly_name: op for op in net_ops}

    @staticmethod
    def entry_index(side, coord, classes, location, entry):
        side_power_2 = side ** 2
        n = location // side_power_2
        loc = location % side_power_2
        return int(side_power_2 * (n * (coord + classes + 1) + entry) + loc)


    @staticmethod
    def scale_bbox(x, y, h, w, class_id, confidence, h_scale, w_scale):
        xmin = int((x - w / 2) * w_scale)
        ymin = int((y - h / 2) * h_scale)
        xmax = int(xmin + w * w_scale)
        ymax = int(ymin + h * h_scale)
        return dict(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, class_id=class_id, confidence=confidence)


    # TODO: Further optimization requires to optimize this function
    @staticmethod
    def parse_yolo_region(blob, resized_image_shape, original_im_shape, params, threshold):
        # ------------------------------------------ Validating output parameters ------------------------------------------
        _, _, out_blob_h, out_blob_w = blob.shape
        assert out_blob_w == out_blob_h, "Invalid size of output blob. It sould be in NCHW layout and height should " \
                                     "be equal to width. Current height = {}, current width = {}" \
                                     "".format(out_blob_h, out_blob_w)

        # ------------------------------------------ Extracting layer parameters -------------------------------------------
        orig_im_h, orig_im_w = original_im_shape
        resized_image_h, resized_image_w = resized_image_shape
        objects = list()
        predictions = blob.flatten()
        side_square = params.side * params.side

        # ------------------------------------------- Parsing YOLO Region output -------------------------------------------
        for i in range(side_square):
            row = i // params.side
            col = i % params.side
            for n in range(params.num):
                obj_index = ObjectRecognizer.entry_index(params.side, params.coords, params.classes, n * side_square + i, params.coords)
                scale = predictions[obj_index]
                if scale < threshold:
                    continue
                box_index = ObjectRecognizer.entry_index(params.side, params.coords, params.classes, n * side_square + i, 0)
                # Network produces location predictions in absolute coordinates of feature maps.
                # Scale it to relative coordinates.
                x = (col + predictions[box_index + 0 * side_square]) / params.side
                y = (row + predictions[box_index + 1 * side_square]) / params.side
                # Value for exp is very big number in some cases so following construction is using here
                try:
                    w_exp = exp(predictions[box_index + 2 * side_square])
                    h_exp = exp(predictions[box_index + 3 * side_square])
                except OverflowError:
                    continue
                # Depends on topology we need to normalize sizes by feature maps (up to YOLOv3) or by input shape (YOLOv3)
                w = w_exp * params.anchors[2 * n] / (resized_image_w if params.isYoloV3 else params.side)
                h = h_exp * params.anchors[2 * n + 1] / (resized_image_h if params.isYoloV3 else params.side)
                for j in range(params.classes):
                    class_index = ObjectRecognizer.entry_index(params.side, params.coords, params.classes, n * side_square + i,
                                                               params.coords + 1 + j)
                    confidence = scale * predictions[class_index]
                    if confidence < threshold:
                        continue
                    objects.append(ObjectRecognizer.scale_bbox(x=x, y=y, h=h, w=w, class_id=j, confidence=confidence,
                                              h_scale=orig_im_h, w_scale=orig_im_w))
        return objects

    def process_frame(self, frame):

        # Prepocess frame
        orig_h, orig_w = frame.shape[:-1]
        frame = frame.transpose((2, 0, 1)) # HWC to CHW
        frame = np.expand_dims(frame, axis=0)

        # Do inference
        # start_time = time()
        self.yolo_model.start_async([frame])
        # det_time = time() - start_time

        # Parse the output
        # t1 = time()
        objects = list()
        if self.yolo_model.wait_for_previous_batch(-1) == 0:
            outputs = self.yolo_model.get_raw_previous_outputs()[0]
            for layer_name, out_blob in outputs.items():

                first_parent_layer = self.net_layers[layer_name].inputs()[0].get_source_output().get_node()

                out_blob = out_blob.reshape(self.net_layers[first_parent_layer.friendly_name].shape)
                layer_params = YoloParams(self.net_layers[layer_name]._get_attributes(), out_blob.shape[2])

                # log.info("Layer {} parameters: ".format(layer_name))
                # layer_params.log_params()
                objects += ObjectRecognizer.parse_yolo_region(out_blob, self.yolo_model.get_input_shape()[2:],
                                             frame.shape[2:], layer_params,
                                             self.prob_threshold)
        # parse_time = time() - t1

        # Filtering overlapping boxes with respect to the iou_threshold
        objects = sorted(objects, key=lambda obj : obj['confidence'], reverse=True)
        for i in range(len(objects)):
            if objects[i]['confidence'] == 0:
                continue
            for j in range(i + 1, len(objects)):
                if intersection_over_union(objects[i], objects[j]) > self.iou_threshold:
                    objects[j]['confidence'] = 0

        # Throwing away filtered boxes with 0 confidence
        objects = [obj for obj in objects if obj['confidence'] >= self.prob_threshold]

        results = list()
        for object in objects:
            results.append({
                "bbox": {
                    "xmin": object["xmin"],
                    "ymin": object["ymin"],
                    "xmax": object["xmax"],
                    "ymax": object["ymax"]
                },
                "class_id": object["class_id"],
                "confidence": object["confidence"]
            })

        # wallclock = time() - start_time

        # There's no need in this information as long parsing stage consuming most of the time
        # self.logger("""OBJECT RECOGNITION PERF STATS: \n\t\tDetection: {:.4f} ms\n
        #                                                 \t\tParse:     {:.4f} ms\n
        #                                                 \t\tWallclock: {:.4f} ms""".format(
        #                                                     det_time*100,
        #                                                     parse_time*100,
        #                                                     wallclock*100
        #                                                 ))

        return results

