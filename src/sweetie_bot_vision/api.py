# -*- coding: utf-8 -*-

import sys, os, time
import cv2 as cv
import numpy as np
import logging as log

from face_recognition import FaceRecognizer, FaceRecognizerConfig
from object_recognition import ObjectRecognizer, ObjectRecognizerConfig

from openvino.inference_engine import IECore

package_location = "/home/cromvell/ros/sweetie_cv/src/sweetie_vision"
def init_models(device="CPU"):

    face_recognizer_config = FaceRecognizerConfig(

        # detection_model_xml = package_location + "/models/face_detection/FP16/face-detection-retail-0004.xml",
        # detection_model_xml = package_location + "/models/face-detection-0105/FP16/face-detection-0105.xml",
        # detection_model_xml = package_location + "/models/face-detection-0106/FP16/face-detection-0106.xml",
        detection_model_xml = package_location + "/models/face-detection-adas-0001/FP16/face-detection-adas-0001.xml",

        reident_model_xml = package_location + "/models/face_reidentification/FP16/face-reidentification-retail-0095.xml",
        landmarks_model_xml = package_location + "/models/landmarks_regression/FP16/landmarks-regression-retail-0009.xml",

        prob_threshold = 0.5,
        roi_scale_factor = 1.15,
        device = device
    )

    object_recognizer_config = ObjectRecognizerConfig(

        yolo_model_xml = package_location + "/models/object_recognition/yolov3-tiny_3l_416.xml",

        prob_threshold = 0.25,
        iou_threshold = 0.4,
        device = device
    )

    log.basicConfig(format="[ %(levelname)s ] %(message)s", level=log.INFO, stream=sys.stdout)

    # Initialization
    ie = IECore()

    face_recognizer = FaceRecognizer(ie, face_recognizer_config)

    object_recognizer = ObjectRecognizer(ie, object_recognizer_config)

    labels_file = os.path.splitext(object_recognizer_config.yolo_model_xml)[0] + ".labels"
    object_class_labels = get_class_labels(labels_file)

    return face_recognizer, object_recognizer, object_class_labels

def process_image(image_path, out_path, device="CPU"):

    if not(type(image_path) is str):
        log.error("Path to the image must be valid string")
        return False, False

    if not(type(out_path) is str):
        log.error("Output path must be valid string")
        return False, False

    # We're reinitializing openvino environment on each call so to avoid
    # synchronization problems between asynchronous requests to the models
    face_recognizer, object_recognizer, object_class_labels = init_models(device)

    log.info("Start processing image")
    image = cv.imread(image_path)

    face_recognizer.process_frame(image)
    face_recognizer.process_frame(image)
    face_detections = face_recognizer.process_frame(image)

    object_recognizer.process_frame(image)
    object_detections = object_recognizer.process_frame(image)

    results = face_detections + object_detections

    results_non_empty = True if len(results) != 0 else False
    for result in results:
        if result in face_detections:
            text = "face_id={}".format('Unknown' if result["id"] == None else result["id"])
        else:
            if object_class_labels == None:
                text = "object_id={}".format(id)
            else:
                text = str(object_class_labels[result["class_id"]])

        draw_bbox(image, text, result["bbox"])

    cv.imwrite(out_path, image)

    return True, results_non_empty

def process_video(video_path, out_path, codec="DIVX", device="CPU"):
    if not(type(video_path) is str) and video_path.startwith("cam"):
        log.error("Path to the video must be valid string and not 'cam'")
        return False, False

    if not(type(out_path) is str):
        log.error("Output path must be valid string")
        return False, False

    # We're reinitializing openvino environment on each call so to avoid
    # synchronization problems between asynchronous requests to the models
    face_recognizer, object_recognizer, object_class_labels = init_models(device)

    # Initializing video stream
    cap = cv.VideoCapture(video_path)
    number_input_frames = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    number_input_frames = 1 if number_input_frames != -1 and number_input_frames < 0 else number_input_frames

    fourcc = cv.VideoWriter_fourcc(*codec)
    width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
    out = cv.VideoWriter(out_path, fourcc, 20.0, (width,height))

    log.info("Start processing video stream. Number of input frames: {}".format(number_input_frames))

    # Number of frames in picture is 1 and this will be read in cycle. Sync mode is default value for this case
    is_async_mode = True
    if number_input_frames != 1:
        ret, frame = cap.read()
    else:
        is_async_mode = False
        wait_key_code = 0

    is_first_iteration = True
    results_non_empty = False
    while cap.isOpened():

        # Read and process input frames
        success, frame = cap.read()

        if not success:
            break

        face_detections = face_recognizer.process_frame(frame)

        object_detections = object_recognizer.process_frame(frame)

        if is_async_mode and is_first_iteration:
            is_first_iteration = False
            continue

        results = face_detections + object_detections

        if not results_non_empty:
            results_non_empty = True if len(results) != 0 else False

        for result in results:
            if result in face_detections:
                text = "face_id={}".format('Unknown' if result["id"] == None else result["id"])
            else:
                if object_class_labels == None:
                    text = "object_id={}".format(id)
                else:
                    text = str(object_class_labels[result["class_id"]])

            draw_bbox(frame, text, result["bbox"])

        out.write(frame)

    cap.release()
    out.release()

    return True, results_non_empty

def get_class_labels(labels_file):
    try:
        with open(labels_file, 'r') as f:
            labels = [ label for label in f.read().split('\n') if label != "" ]

            return labels

    except (IOError) as e:
        log.warn("Cannot open object labels file. Displaying raw class ids instead")

        return None

def draw_bbox(image, text, rect):
    # Draw bounding box
    cv.rectangle(image, (rect["x_min"], rect["y_min"]), (rect["x_max"], rect["y_max"]), (0, 220, 0), 2)

    # Draw text on the background
    origin = np.array((rect["x_min"], rect["y_min"]))
    scale = 0.5
    text_size, baseline = cv.getTextSize(text, cv.FONT_HERSHEY_SIMPLEX, scale, 1)

    if origin[1]-text_size[1] <= 0:
        origin += (0, text_size[1])

    cv.rectangle(image,
                 tuple((origin + (0, baseline)).astype(int)),
                 tuple((origin + (text_size[0], -text_size[1])).astype(int)),
                 (255, 255, 255), cv.FILLED)
    cv.putText(image, text, tuple(origin.astype(int)), cv.FONT_HERSHEY_SIMPLEX, scale, 0, 1, cv.LINE_AA)
