
import cv2 as cv
import numpy as np
from mediapipe import solutions
from mediapipe.framework.formats import landmark_pb2

def resize_frame(frame, to_shape):
    n, c, h, w = to_shape

    input = frame[0]
    if input.shape[:-1] != (h, w):
        input = input.transpose((1, 2, 0)) # to HWC
        input = cv.resize(input, (w, h))
        input = input.transpose((2, 0, 1)) # Change data layout from HWC to CHW

    return input.reshape((n, c, h, w))


def resize_frames(frames, to_shape):
    return [resize_frame(frame, to_shape) for frame in frames]

def crop_roi_with_copy(frame, roi):
    return np.array(frame[:, :, roi["ymin"]:roi["ymax"], roi["xmin"]:roi["xmax"]])

def crop_rois_with_copy(frame, rois):
    return [crop_roi_with_copy(frame, roi) for roi in rois]


def rescale_roi(roi, roi_scale_factor=1.0):
    rescaled_roi = roi.copy()
    x_min, y_min, x_max, y_max = [ rescaled_roi[k] for k in ["xmin", "ymin", "xmax", "ymax"] ]

    point1 = np.array((x_min, y_min), dtype=np.float64)
    point2 = np.array((x_max, y_max), dtype=np.float64)
    size = np.array((x_max - x_min, y_max - y_min), dtype=np.float64)

    point1 -= size * 0.5 * (roi_scale_factor - 1.0)
    point2 += size * 0.5 * (roi_scale_factor - 1.0)

    point1[0] = max(0, point1[0])
    point1[1] = max(0, point1[1])
    # @Warning: Don't clip point2 against image size. Strange things happens

    rescaled_roi["xmin"] = int(point1[0])
    rescaled_roi["ymin"] = int(point1[1])
    rescaled_roi["xmax"] = int(point2[0])
    rescaled_roi["ymax"] = int(point2[1])
    return rescaled_roi


def rescale_rois(rois, roi_scale_factor=1.0):
    return [rescale_roi(roi, roi_scale_factor) for roi in rois]


def landmarks_error(groundtruth, prediction):
    assert np.size(groundtruth) == np.size(prediction), "Landmark vectors size not equal"
    N = len(groundtruth)

    interocular_distance = np.linalg.norm(np.array(groundtruth[0]) - np.array(groundtruth[1]))
    sum = 0
    for i in range(N):
        sum += np.linalg.norm(np.array(groundtruth[i]) - np.array(prediction[i])) / interocular_distance

    return sum / N


def intersection_over_union(box_1, box_2):
    width_of_overlap_area = min(box_1['xmax'], box_2['xmax']) - max(box_1['xmin'], box_2['xmin'])
    height_of_overlap_area = min(box_1['ymax'], box_2['ymax']) - max(box_1['ymin'], box_2['ymin'])
    if width_of_overlap_area < 0 or height_of_overlap_area < 0:
        area_of_overlap = 0
    else:
        area_of_overlap = width_of_overlap_area * height_of_overlap_area
    box_1_area = (box_1['ymax'] - box_1['ymin']) * (box_1['xmax'] - box_1['xmin'])
    box_2_area = (box_2['ymax'] - box_2['ymin']) * (box_2['xmax'] - box_2['xmin'])
    area_of_union = box_1_area + box_2_area - area_of_overlap
    if area_of_union == 0:
        return 0
    return area_of_overlap / area_of_union


def draw_bbox(frame, res, id_key, text_generator, label_on_inside = False):
    id = res[id_key]
    box = res["bbox"]

    # Draw face bounding box
    cv.rectangle(frame, (box["xmin"], box["ymin"]), (box["xmax"], box["ymax"]), (0, 220, 0), 2)

    # Draw id on background
    text = text_generator(id)
    origin = np.array((box["xmin"], box["ymin"]))
    scale = 0.5
    text_size, baseline = cv.getTextSize(text, cv.FONT_HERSHEY_SIMPLEX, scale, 1)
    if label_on_inside:
        cv.rectangle(frame,
                     tuple(origin.astype(int)),
                     tuple((origin + (text_size[0], text_size[1])).astype(int)),
                     (255, 255, 255), cv.FILLED)
        cv.putText(frame, text, tuple((origin + (0, text_size[1])).astype(int)), cv.FONT_HERSHEY_SIMPLEX, scale, 0, 1, cv.LINE_AA)
    else:
        cv.rectangle(frame,
                     tuple((origin + (0, baseline)).astype(int)),
                     tuple((origin + (text_size[0], -text_size[1])).astype(int)),
                     (255, 255, 255), cv.FILLED)
        cv.putText(frame, text, tuple(origin.astype(int)), cv.FONT_HERSHEY_SIMPLEX, scale, 0, 1, cv.LINE_AA)


def draw_qr(frame, detection):
    points = detection.polygon
    bbox   = detection.rect

    if len(points) > 4:
        hull = cv.convexHull(np.array([point for point in points], dtype=np.float32))
        hull = list(map(tuple, np.squeeze(hull)))
    else:
        hull = points

    n = len(hull)

    # print("Type: {}".format(detection.type))
    # print("Data: {}".format(detection.data))
    for j in range(0, n):
        cv.line(frame, hull[j], hull[(j+1) % n], (255, 0, 0), 3)

    text = "qr: {}".format(detection.data.decode('utf-8'))
    scale = 0.5
    origin = np.array((bbox.left, bbox.top))
    text_size, baseline = cv.getTextSize(text, cv.FONT_HERSHEY_SIMPLEX, scale, 1)
    cv.rectangle(frame,
                 tuple((origin + (0, baseline)).astype(int)),
                 tuple((origin + (text_size[0], -text_size[1])).astype(int)),
                 (255, 255, 255), cv.FILLED)
    cv.putText(frame, text, tuple(origin.astype(int)), cv.FONT_HERSHEY_SIMPLEX, scale, 0, 1, cv.LINE_AA)

def get_class_labels(labels_file, logger=print):
    try:
        with open(labels_file, 'r') as f:
            labels = [ label for label in f.read().split('\n') if label != "" ]

            return labels

    except (IOError) as e:
        logger("Cannot open object labels file. Displaying raw class ids instead")

        return None

MARGIN = 10  # pixels
FONT_SIZE = 1
FONT_THICKNESS = 1
HANDEDNESS_TEXT_COLOR = (88, 205, 54) # vibrant green

def draw_landmarks_on_image(frame, detection_result):
  hand_landmarks_list = detection_result.hand_landmarks
  handedness_list = detection_result.handedness

  # Loop through the detected hands to visualize.
  for idx in range(len(hand_landmarks_list)):
    hand_landmarks = hand_landmarks_list[idx]
    handedness = handedness_list[idx]

    # Draw the hand landmarks.
    hand_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
    hand_landmarks_proto.landmark.extend([
      landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in hand_landmarks
    ])
    solutions.drawing_utils.draw_landmarks(
      frame,
      hand_landmarks_proto,
      solutions.hands.HAND_CONNECTIONS,
      solutions.drawing_styles.get_default_hand_landmarks_style(),
      solutions.drawing_styles.get_default_hand_connections_style())

    # Get the top left corner of the detected hand's bounding box.
    height, width, _ = frame.shape
    x_coordinates = [landmark.x for landmark in hand_landmarks]
    y_coordinates = [landmark.y for landmark in hand_landmarks]
    text_x = int(min(x_coordinates) * width)
    text_y = int(min(y_coordinates) * height) - MARGIN

    # Draw handedness (left or right hand) on the image.
    cv.putText(frame, f"{handedness[0].category_name}",
                (text_x, text_y), cv.FONT_HERSHEY_DUPLEX,
                FONT_SIZE, HANDEDNESS_TEXT_COLOR, FONT_THICKNESS, cv.LINE_AA)
