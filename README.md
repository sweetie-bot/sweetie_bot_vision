
**Note:** This repository is a part of Sweetie Bot Project (https://gitlab.com/sweetie-bot/sweetie_bot). Although vision system was designed specifically for the robot, it also can be used in various applications. 

![](doc/figures/recognition_examples.png)

## Installation

Install OpenVino 2020 or later.

Create ROS workspace or add the package to the existing one:
```
mkdir -p ~/ros/sweetie_bot_cv/src
cd ~/ros/sweetie_bot_cv/src
git clone https://gitlab.com/sweetie-bot/sweetie_bot_vision.git
```

Install python dependencies:
```
cd ~/ros/sweetie_bot_cv/src/sweetie_bot_vision
pip3 install -r requirements.txt
```

Build workspace:
```
cd ~/ros/sweetie_bot_cv
source /opt/ros/sweetie_bot/setup.bash
catkin_make
``` 

## Usage

Set ROS and OpenVino environments:
```
source ~/ros/sweetie_bot_cv/devel/setup.bash
source /opt/intel/openvino/bin/setupvars.sh
```

### Run demo

```
rosrun sweetie_bot_vision demo /path/to/a/video.mp4 [DEVICE_NAME]
```

or

```
rosrun sweetie_bot_vision demo [cam] [DEVICE_NAME]
```

### Run vision node

```
roslaunch sweetie_bot_vision vision.launch
```

Params at `launch/vision.yaml`

Names of published topics are `detections` (`DetectionArray`) and `hmi/detections` (`MarkerArray`)
