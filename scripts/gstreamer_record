#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, re, datetime, time, subprocess

# Params

output_dir = os.path.expanduser("~") + "/gstreamer_recordings/"
video_max_length = 3600 # in seconds

if len(sys.argv) > 1 and not sys.argv[1].startswith("__"):
    if sys.argv[1] == "--help" or sys.argv[1] == "-h" or sys.argv[1] == "-help":
        print("Usage: rosrun sweetie_bot_vision gstreamer_record [/path/to/recordings/folder/] [max_video_length_in_seconds]")
        exit(0)
    else:
        output_dir = sys.argv[1]

if len(sys.argv) > 2 and not sys.argv[2].startswith("__"):
    try:
        video_max_length = int(sys.argv[2])
    except:
        print("Error: second parameter must be a integer")
        exit(1)
    
if not os.path.exists(output_dir):
    if os.path.isdir(output_dir):
        print("Cannot create output dir ({}). File with the same name exists".format(output_dir))
        exit(1)

    os.mkdir(output_dir)
    

files = []
for (_, _, filenames) in os.walk(output_dir):
    files.extend(filenames)
    break

max_video_number = -1
rx = r"recording_([0-9]+)_.+"
for filename in files:
    regex_result = re.search(rx, filename)
    if regex_result:
        video_number = int(regex_result.group(1))
        if video_number > max_video_number:
            max_video_number = video_number

while True:
    video_name = "recording_{}_{}.mkv".format(max_video_number + 1, datetime.datetime.now().strftime("%Y-%m-%d_%X"))

    print("Init string")
    gst_str = "gst-launch-1.0 udpsrc port=5001 ! application/x-rtp, encoding-name=JPEG,payload=26 ! rtpjpegdepay ! jpegdec ! videoconvert ! matroskamux name=mkv ! filesink location={}".format(os.path.join(output_dir, video_name))

    print("Starting subprocess")
    # Start gstreamer in subprocess
    gst_process = subprocess.Popen(gst_str.split())

    # Wait for a timeout and restart recording to the new video
    time.sleep(video_max_length)

    gst_process.kill()
    max_video_number += 1
